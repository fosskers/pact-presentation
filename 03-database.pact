(define-keyset 'admin-keyset (read-keyset "admin-keyset"))

(module databaseExample 'admin-keyset
  "Manage a simple database of 'accounts'."

  ;; 簡単な「型指定」
  (defconst cats:integer 5)

  ;; 関数にも型指定
  (defun oneMore:integer (n:integer)
    "Add one!"
    (+ n 1))

  ;; テーブルの列の型
  (defschema account
    name:string
    age:integer
    money:integer)

  ;; テーブルを宣言し、スキーマー（これも”型”）を与える
  (deftable accounts:{account})

  (defun new-account (name:string age:integer)
    "新しい高座を作る。"
    (insert accounts name { "name": name, "age": age, "money": 0 }))

  ;; バッグあり。危険性もある。
  (defun give-money (name:string extra:integer)
    "高座に入金。"
    (with-read accounts name { "money" := m }
               (update accounts name { "money": (+ m extra) })))

  )

;; 必須
(create-table accounts)
