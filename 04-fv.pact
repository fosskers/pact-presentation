(define-keyset 'admin-keyset (read-keyset "admin-keyset"))

(module fvExample 'admin-keyset
  "Simple Formal Verification"

  ;; よし、仕事した。帰ろう。
  (defun abs:integer (n:integer)
    @doc "整数の絶対値"
    ;; @model [(property (>= result 0))]
    (if (< n 0)
        (* n 1)
      n)
    )

  ;; テーブルの列の型
  (defschema account
    @doc "残高のある高座のようなもの"
    ;; @model [(invariant (>= money 0))]

    name:string
    age:integer
    money:integer
    ks:keyset)

  ;; テーブルを宣言し、スキーマー（これも”型”）を与える
  (deftable accounts:{account})

  (defun new-account (name:string age:integer ks:keyset)
    "新しい高座を作る。"
    (insert accounts name
      { "name": name, "age": age, "money": 0, "ks": ks }))

  ;; バッグあり。危険性もある。
  (defun give-money (name:string extra:integer)
    @doc "高座に入金。"
    ;; @model [(property (row-enforced accounts 'ks name))]
    ;; (enforce (>= extra 0) "Value to add was not positive!")
    (with-read accounts name { "money" := m, "ks" := ks }
               ;; (enforce-keyset ks)
               (update accounts name { "money": (+ m extra) })))

  )

;; 必須
(create-table accounts)
