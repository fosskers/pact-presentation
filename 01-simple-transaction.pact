(define-keyset 'admin-keyset (read-keyset "admin-keyset"))

(module helloNode 'admin-keyset
  "A module that can say hi."

  (defun hello (name)
    "Say it!"
    (format "Hello {}!" [name]))
  )
